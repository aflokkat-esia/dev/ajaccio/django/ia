## Liste des commandes pour créer un projet Django

```bash
## Création du répertoire allant contenir notre project Django

mkdir my-django-project
cd my-django-project
```

---

```bash
## Création du projet Django

# Avec poetry
poetry init
poetry add django
poetry run django-admin startproject project .

# Sans poetry
python -m venv .venv
.\.venv\Scripts\activate
pip install django
django-admin startproject project .
```

---

```bash
## Execution du projet

# Avec poetry
poetry run python manage.py runserver

# Sans poetry
python manage.py runserver
```

---

```bash
## Création d'une application

# Avec poetry
poetry run python manage.py startapp afloapp

# Sans poetry
python manage.py startapp afloapp
```

---

```bash
# Ajout du nom de l'application à la variable INSTALLED_APPS du ficher settings.py du répertoire project

# Inclusion des urls de l'application dans les urls du projet avec la fonction include du module django.urls
```
