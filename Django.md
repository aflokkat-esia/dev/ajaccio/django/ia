# Django
---
### Initialisation
---
Après avoir configurer un environnement virtuel avec `python -m venv venv` par exemple, on installe Django avec `pip install django`. 
`django-admin` est un outil qui nous permet d'effectuer un certain nombre d'actions. Pour créer un projet (contenant plusieurs applications) :

```bash
django-admin startproject myproject
```

Ensuite, pour lancer le projet :

```bash
cd myproject
python manage.py runserver
```

Le fichier `settings.py` regroupe toute la configuration de notre projet. C'est l'élément central.

Pour créer une application au sein de ce projet :

```bash
python manage.py startapp myapp
```

Ensuite, ajouter `'myapp.apps.BaseConfig'` au fichier `settings.py` à la suite de INSTALLED_APPS.

### Routes
---
Le fichier `urls.py` du répertoire `myproject` est le fichier principal pour les urls, mais l'application `myapp` aura aussi le sien.

```python
...
from django.http import HttpResponse

def home(request):
	return HttpResponse("Home page")

urlpatterns = [
	...
	path('home/', home),
]
```

Néanmoins, il est intéressant de séparer les routes des différentes applications de notre projet. Pour cela, on crée un fichier `urls.py` dans l'application qui comportera les urls liées à cette application.

```python
from django.urls import path
from . import views

urlpatterns = [
	path('', views.home, name='home'),
	path('room/', views.room, name='room')
]
```

Les fonctions utilisées précédemment sont alors déplacées dans `views.py`.

```python
...

def home(request):
	return HttpResponse("Home page")

def room(request):
	return HttpResponse("Room page")
```

Dans le fichier `urls.py` de `myproject`, on inclut les chemins d'accès définis dans l'application.

```python
...
from django.urls import include

urlpatterns = [
	...
	path('', include('base.urls'))
]
```

### Templates
---
Les templates vont être stockées dans un répertoire `templates` à la racine de l'arborescence. Ce répertoire doit être renseigné dans le fichier `settings.py` de `myproject`.

```python
TEMPLATES = [
	{
		...
		'DIRS': [
			BASE_DIR / 'templates'
		],
		...
	}
]
```

Alors, les templates sont utilisées dans le fichier `views.py` de l'application.

```python
...

def home(request):
	return render(request, 'home.html')
```

Il est possible d'inclure des composants HTML dans des templates.

```django
{% include 'navbar.html' %}

<h1>Home</h1>
```

Cependant, il est plus logique de créer un template "layout" qui regroupera tous les éléments communs aux pages et permettra d'insérer des éléments spécifiques aux pages dedans.

```django
...
<body>
	{% include 'navbar.html' %}
	{% block content %}

	{% endblock %}
</body>
...
```

Alors, un fichier "enfant" aura la forme suivante :

```django
{% extends "main.html" %}

{% block content %}

<h1>Enfant</h1>

{% endblock %}
```

Il est possible de passer des données aux templates.

```python
rooms = [
	{ 'id': 1, 'name': 'Lets learn Python' },
	{ 'id': 2, 'name': 'Lets learn Django' },
]

def home(request):
	return render(request, 'home.html', { 'rooms': rooms })
```

Dans le fichier `home.html`, on peut afficher ces données avec une boucle `for`.

```django
...
<div>
	{% for room in rooms %}
		<p>{{ room.id }} -- {{ room.name }}</p>
	{% endfor %}
</div>
...
```

Cependant, il est plus propre de créer un dictionnaire `context` plutôt que de passer les données séparemment.

```python
...
def home(request):
	context = { 'rooms': rooms }
	return render(request, 'home.html', context)
```

Le répertoire `templates` du répertoire `myproject` comporte uniquement les templates communs à nos différentes applications. Les templates spécifiques sont alors stockées dans un répertoire `templates` au sein de l'application. On crée un autre répertoire du nom de l'application au sein de ce répertoire `templates`.

```
myapp
	- templates
		- myapp 
```

Alors, le fichier `views.py` est modifié.

```python
def home(request):
	...
	return render(request, 'myapp/home.html', context)
```

Il est possible de modifier l'url d'une page pour y inclure un paramètre. On le place entre chevrons <> et on spécifie son type, ici `str`.

```python
...
urlpatterns = [
	...
	path('room/<str:pk>/', views.room, name='room')
]
```

La fonction `views.py` doit alors prendre en compte ce paramètre.

```python
def room(request, pk):
	room = None
	for i in rooms:
		if i['id'] == int(pk):
			room = i
	context = { 'room': room }
	return render(request, 'myapp/room.html', context)
```

Le fichier `room.html` devient alors

```django
{% extends 'main.html' %}

{% block content %}

<h1>{{ room.name }}</h1>

{% endblock %}
```

Il est plus intéressant d'utiliser le nom d'une Url plutôt que l'Url elle-même pour faire un lien dans le fichier `home.html`.

```django
...
<a href="{% url 'room' room.id %}">{{ room.name }}</a>
...
```

### Databases et Administration Panel
---
Pour faire migrer les modifications apportées à la base de données, on utilise la commande `python manage.py migrate`. La base de données est alors créée ou modifiée.

Les modèles de la base de données sont stockés dans le fichier `models.py` de l'application. Les tables de la base de données sont représentées par des classes python.

```python
from django.db import models

class Room(models.Model):
	name = models.CharField(max_length=200)
	description = models.TextField(null=True, blank=True)
	# blank is for the form we are going to submit
	# null is for the database
	updated = models.DateTimeField(auto_now=True)
	created = models.DateTitmeField(auto_now_add=True)

	def __str__(self):
		return self.name
```

Une fois les modifications écrites, on crée une migration à l'aide de la commande `python manage.py makemigrations`.

Pour accéder au panel d'administration, on crée un super utilisateur avec la commande `python manage.py createsuperuser`. Alors, on peut accéder au panel via l'Url `localhost:8000/admin`.

Pour pouvoir voir les tables créées dans le panel, il faut rajouter une partie configuration au fichier `admin.py` de l'application.

```python
from django.contrib import admin
from .models import Room

admin.site.register(Room)
```

On récupère maintenant les données de la base dans le fichier `views.py` que l'on transmet à la page HTML.

```python
...
from .models import Room
...

def home(request):
	rooms = Room.objects.all()
	...
```

On peut récupérer les données d'un seul objet en spécifiant son identifiant, qui, par défaut, est un entier.

```python
...
from .models import Room
...

def room(request, pk):
	rooms = Room.objects.get(id=pk)
	...
```

### Relations
---
Il est possible de définir des relations entre les tables de la base de données.

```python
class Message(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	room = models.ForeignKey(Room, on_delete=models.CASCADE)
```

Ci-dessus, la relation est une relation One-To-Many : une room peut "avoir" plusieurs messages mais un message n'appartient qu'à une room. Le paramètre `on_delete` permet de spécifier ce qu'il se passe lorsque l'élément de la table liée est supprimé (`SET_NULL`, `CASCADE`). Dans le cas où ce paramètre est mis à `SET_NULL`, il faut aussi rajouter le paramètre `null=True` pour permettre au champ de la table de prendre une valeur `null` si besoin.

Django propose par défaut un modèle pour l'utilisateur. Pour l'utiliser, il suffit de l'importer : 
`from django.contrib.auth.models import User`.

Rappel : l'ajout de nouvelles tables doit être spécifié dans le fichier `admin.py` .

### Formulaire
---
Dans un nouveau fichier `room_form.html`, nous allons créer un formulaire.

```django
{% entends 'main.html' %} 

{% block content %}
	<div>
		<form method="POST" action="">
			{% csrf_token %}
			
			<input type="submit" value="Submit" />
		</form>
	</div>
{% endblock %}
```

Cependant, pour créer des formulaires, il est plus pratique d'utiliser une représentation sous forme de classe et une fonctionnalité de Django. Dans un fichier `forms.py` :

```python
from django.forms import ModelForm
from .models import Room

class RoomForm(ModelForm):
	class Meta:
		model = Room
		fields = ['name', 'body'] # or '__all__'
```

Alors, on peut créer une route dans `urls.py` et une vue dans `views.py` qui vont correspondre au formulaire.

```python
from .forms import RoomForm

def createRoom(request):
	form = RoomForm()
	context = { 'form': form }
	return render(request, 'base/room_form.html', context)
```

Alors, on appelle le formulaire dans le fichier html correspondant à l'aide de `{{form}}` ou `{{form.as_p}}`.

Ensuite, pour récupérer les données du formulaire, on peut modifier la vue précédente.

```python
def createRoom(request):
	...
	if request.method == "POST":
		print(request.POST.get("name"))
	...
```

Pour sauvegarder directement les données récupérées du formulaire dans la base de données, on peut utiliser la syntaxe suivante :

```python
from django.shortcuts import redirect
def createRoom(request):
	...
	if request.method == "POST":
		form = RoomForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('home')
	...
```

### CRUD
---
CRUD is for Create, Read (or Request), Update and Delete. Il s'agit des 4 opérations que l'on effectue sur le Web et qui sont la base des applications.

Remarque : lorsque l'on récupère des informations, il est possible de trier les données à l'aide de la classe Meta :

```python
class Room():
	...
	class Meta:
		ordering = ['-updated', '-created']
```

Ici, les éléments sont retrournés par date de modification inverse puis par date de création inverse pour récupérer les éléments les plus récents en premier.

La création de données a déjà été traitée dans la partie sur les formulaires. La récupération des données un peu plus tôt. Voyons la modification des données avec la vue à créer.

```python
def updateRoom(request, pk):
	room = Room.objects.get(id=pk)
	form = RoomForm(instance=room)
	
	if request.method == 'POST':
		form = RoomForm(request.POST, instance=room)
	if form.is_valid:
		form.save()
	return redirect('home')
	
	context = { 'form': form }
	return render(request, 'base/room_form.html', context)
```

Il faut alors rajouter un nouvel url dans `urls.py` : 
`path('update_room/<str:pk>/', views.updateRoom, name="update-room")`.

De même pour la suppression d'une donnée.

```python
def deleteRoom(request, pk):
	room = Room.objects.get(id=pk)
	if request.method == "POST":
		room.delete()
		return redirect("home")
	return render(request, 'base/delete.html', { 'obj': room })
```

### Query params
---
Il est possible de récupérer des paramètres dans les urls à l'aide de la syntaxe :

```python
def home(request):
	q = request.GET.get('q') if request.GET.get('q') != None else ''
	rooms = Room.objects.filter(topic__name=q)
	# or
	rooms = Room.objects.filter(topic__name__contains=q)
```

Ici, on filtre les rooms à partir du nom de leur sujet avec la syntaxe particulière des doubles underscores.

### Authentification
---
Nous allons à présent mettre en place un système d'authentification. L'idée est de disposer d'un lien pour se connecter avec des identifiants et d'un pour se déconnecter. Django propose des fonctions par défaut qui permettent de réaliser ces tâches. Les vues de connexion et de déconnexion peuvent se présenter sous cette forme (en faisant appel aux messages flashs qui sont aussi des fonctionnalités par défaut de Django).

```python
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def loginPage(request):
	if request.method == "POST":
		username = request.POST.get('username')
		password = request.POST.get('password')
	  
		try:
			user = User.objects.get(username=username)
		except:
			messages.error(request, 'User does not exist...')
		
		user = authenticate(request, username=username, password=password)
		
		if user is not None:
			login(request, user)
			return redirect('home')
		else:
			messages.error(request, 'Username ou password does not exist...')
			context = {}
		
return render(request, 'base/login_register.html', context)
```

La connexion crée un cookie qui stocke la session active avec l'utilisateur connecté. Pour se déconnecter :

```python
def logoutUser(request):
	logout(request)
	return redirect('home')
```

### Autorisation
---
Il est possible de définir des autorisations pour certaines pages de manière à ce que seules les utilisateurs connectés puissent y accéder. Dans le fichier `views.py`, on peut utiliser le décorateur `@login_required()` de la manière suivante :

```python
from django.contrib.auth.decorators import login_required

@login_required(login_url="/login")
def createRoom(request):
	...
```

### Création de compte
---
Pour créer un compte, on peut utiliser un formulaire proposé par défaut par Django :

```python
from django.contrib.auth.forms import UserCreationForm

def registerPage(request):
	page = 'register'
	form = UserCreationForm()
	return render(request, 'base/login_register.html', { 'page': page, 'form': form })
```

### CRUD
---
Pour récupérer les informations d'une table liée à une autre, on utilise la syntaxe suivante :

```python
def room(request, pk):
	room = Room.objects.get(id=pk)
	messages = room.message_set.all()
	context = { 'room': room, 'messages': messages }
	...
```

Pour créer une relation Many-to-Many, on utilise la syntaxe suivante :

```python
class Room(models.Model):
	...
	participants = models.ManyToManyField(User, related_name="participants", blank=True)
	...
```