from django.contrib import admin
from .models import Formateur, Eleve

# Register your models here.

admin.site.register(Formateur)
admin.site.register(Eleve)