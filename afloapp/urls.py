from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('contact/', views.contact, name="contact"),

    path('register/', views.register, name="register"),
    path('login/', views.login, name="login"),
    path('logout/', views.logout, name="logout"),
    
    path('formateurs/', views.formateurs, name="formateurs"),
    path('formateurs/create/', views.create_formateur, name="create-formateur"),
    path('formateurs/<int:id>/', views.formateur, name="formateur"),
    path('formateurs/update/<int:id>/', views.update_formateur, name="update-formateur"),
    path('formateurs/delete/<int:id>/', views.delete_formateur, name="delete-formateur"),
    path('formateurs/remove-best-formateur/<int:id>/', views.remove_best_formateur, name="remove-best-formateur"),
]