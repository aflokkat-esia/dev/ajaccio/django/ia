from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib.auth import authenticate, login as djangoLogin, logout as djangoLogout
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import UserCreationForm

from .models import Formateur, Eleve
from .forms import FormateurForm, EleveForm


def home(request):
    context = { 
        "name": "Quentin", 
        "age": 30,
        "connected": True,
        "activities": [
            { "name": "Programming", "display": True }, 
            { "name": "Sleeping", "display": True }, 
            { "name": "Eating", "display": False }
        ]
    }
    return render(request, 'afloapp/pages/home.html', context)

def contact(request):
    return render(request, 'afloapp/pages/contact.html')

# Authentication

def register(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid:
            form.save()
            # group = Group.objects.get(name="Utilisateur")
            # user.groups.add(group)
            # user.save()
            return redirect('login')

    form = UserCreationForm()
    return render(request, 'afloapp/pages/register.html', { 'form': form })


def login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        try:
            check_user = User.objects.get(username=username)
        except:
            return redirect('login')
        
        user = authenticate(request, username=username, password=password)
        if user == None:
            return redirect('login')

        djangoLogin(request, user)
        return redirect('formateurs')

    return render(request, 'afloapp/pages/login.html')

@login_required(login_url="login")
def logout(request):
    djangoLogout(request)
    return redirect('login')

# Formateurs

@login_required(login_url="login")
@permission_required(perm="afloapp.view_formateur", login_url="login")
def formateurs(request):
    formateurs = Formateur.objects.all()
    context = { "formateurs": formateurs }
    return render(request, 'afloapp/pages/formateurs.html', context)


@login_required(login_url="login")
@permission_required(perm="afloapp.view_formateur", login_url="login")
def formateur(request, id):
    formateur = Formateur.objects.get(id=id)
    eleves = Eleve.objects.filter(formateur=formateur)
    context = { "formateur": formateur, "eleves": eleves }
    return render(request, 'afloapp/pages/formateur.html', context)


@login_required(login_url="login")
@permission_required(perm="afloapp.add_formateur", login_url="formateurs")
def create_formateur(request):
    if request.method == "POST":
        form = EleveForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('formateurs')

    form = EleveForm()
    context = { "form": form }
    return render(request, 'afloapp/pages/create_formateur.html', context)


@login_required(login_url="login")
@permission_required(perm="afloapp.change_formateur", login_url="formateurs")
def update_formateur(request, id):
    formateur = Formateur.objects.get(id=id)

    if request.method == "POST":
        form = FormateurForm(request.POST, instance=formateur)
        if form.is_valid:
            form.save()
            return redirect('formateurs')

    form = FormateurForm(instance=formateur)
    context = { "form": form }
    return render(request, 'afloapp/pages/update_formateur.html', context)


@login_required(login_url="login")
@permission_required(perm="afloapp.delete_formateur", login_url="formateurs")
def delete_formateur(request, id):
    formateur = Formateur.objects.get(id=id)
    formateur.delete()
    return redirect('formateurs')


@login_required(login_url="login")
def remove_best_formateur(request, id):
    formateur = Formateur.objects.get(id=id)
    formateur.best_formateur = False
    formateur.save()
    return redirect('formateurs')