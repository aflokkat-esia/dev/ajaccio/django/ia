from django.forms import ModelForm
from .models import Formateur, Eleve


class FormateurForm(ModelForm):
    class Meta:
        model = Formateur
        fields = "__all__"


class EleveForm(ModelForm):
    class Meta:
        model = Eleve
        fields = ["name", "formateur"]