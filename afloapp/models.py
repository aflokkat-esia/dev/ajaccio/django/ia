from django.db import models

class Formateur(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(null=True, blank=True)
    age = models.IntegerField(default=30)
    best_formateur = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Eleve(models.Model):
    name = models.CharField(max_length=30)
    formateur = models.ForeignKey(Formateur, related_name="eleves", on_delete=models.CASCADE)
    formateurs = models.ManyToManyField(Formateur, related_name="esclaves")

    def __str__(self):
        return self.name
